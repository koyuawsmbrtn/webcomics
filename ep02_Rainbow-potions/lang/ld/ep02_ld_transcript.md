# Transcript of Pepper&Carrot Episode 02 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 2: Woliri Worana

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thod|1|True|Lob
Thod|2|True|Lob
Thod|3|False|Lob
Thod|4|False|DIBÉE: BELID
Zho|5|True|SHÁTHO
Zho|6|True|YAHANE-
Zho|7|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thod|1|False|zhoma
Thod|2|False|ÓOWA
Thod|3|False|RUHOBEMELA
Thod|4|False|L(H)ULA
Thod|5|False|LEYIHUL
Thod|6|False|LAY
Zho|7|False|OLAYA|nowhitespace
Thod|8|False|RODALA
Thod|9|False|LÉLI
Thod|10|False|LAYUNEHAL
Thod|11|False|ÓOWAMEDA
Thod|12|False|RUHOBEMELA
Thod|13|False|L(H)ULA
Thod|14|False|LEYIHUL
Thod|15|False|LÉELAYAHI
Thod|16|False|LULA Y
Thod|17|False|LOYOLAYA

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zho|1|True|Lob
Zho|2|True|Lob
Zho|3|False|Lob

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zho|1|True|Rilin
Zho|2|False|Rilin
Zho|3|True|Rilin
Zho|4|False|Rilin
Zho|5|True|Rilin
Zho|6|False|Rilin
Zho|7|True|m|nowhitespace
Zho|8|True|m|nowhitespace
Zho|9|True|M|nowhitespace
Zho|10|False|S|nowhitespace
Zho|11|True|H|nowhitespace
Zho|12|True|ú|nowhitespace
Zho|13|True|ul !|nowhitespace
Zho|14|True|S|nowhitespace
Zho|15|True|H|nowhitespace
Zho|16|True|S|nowhitespace
Zho|17|False|H|nowhitespace
Zho|18|True|í|nowhitespace
Zho|19|True|l|nowhitespace
Zho|20|False|e|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zho|1|True|R
Zho|2|True|alob|nowhitespace
Zho|3|True|R
Zho|4|False|alob|nowhitespace
Zho|5|True|a
Zho|6|True|b|nowhitespace
Zho|7|True|a|nowhitespace
Zho|8|True|a
Zho|9|True|b|nowhitespace
Zho|10|True|a|nowhitespace
Zho|11|True|a
Zho|12|True|b|nowhitespace
Zho|13|False|a|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|1|True|Bíi dínonehóo dademelom hi wa. Meden with thabeshin i nede loshenan wubedim hi nuha: n
Dohiná|2|False|www.patreon.com/davidrevoy
Dohiná|3|False|Áala menedebe withedim hi:
Dohiná|4|False|Eleshub Krita benan GNU/Linux beha
