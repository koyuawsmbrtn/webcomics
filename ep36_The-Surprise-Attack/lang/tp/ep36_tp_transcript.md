# Transcript of Pepper&Carrot Episode 36 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka luka wan: utala kama

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Wasapi|1|True|SINA WILE PONA E IKE SINA KEPEKEN TOKI ANU SEME?
jan Wasapi|2|False|ni li ken ala!
jan Wasapi|3|False|O PANA E ONA IKE TAWA POKI AWEN!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Www il e...
jan Pepa|2|True|pakala!
jan Pepa|3|False|mi ken ala pali lon poki ni!
jan Pepa|4|False|poki awen ike pi weka wawa a!
kalama|5|False|KIWEN!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|tan seme la pona li lon pilin mi pi tenpo pini!
jan Sisimi|2|True|jan Pepa o pini!
jan Sisimi|3|False|o kalama ala.
jan Pepa|4|False|jan seme a li lon?!
jan Sisimi|5|True|o pini e kalama kin a!
jan Sisimi|6|True|o kama.
jan Sisimi|7|False|mi wile ken e weka sina.
kalama|8|False|Sssuno...
jan Pepa|9|False|jan Sisimi anu seme?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|True|mi... mi pilin ike tan tenpo pini...
jan Sisimi|2|True|...a, tan utala pi mi tu.
jan Sisimi|3|True|ni...
jan Sisimi|4|False|ni taso li nasin.
jan Pepa|5|True|o pilin ike ala - mi sona.
jan Pepa|6|False|kama sina li pona.
jan Sisimi|7|False|poki ni pi weka wawa li wawa mute – jan awen li pali mute lon sina!
jan Pepa|8|False|a a a!
jan Sisimi|9|False|o lili e kalama. ante la, ona li kute.
Rat|10|True|MOKU
Rat|11|True|MOKU
Rat|12|False|MOKU
jan Sisimi|13|True|a, sama la,
jan Sisimi|14|True|ijo ante li kama e mi. tenpo sewi mi li kama pini la, mi kama jan kulupu sona pi jan Wasapi.
jan Sisimi|15|False|mi kama sona e wile ona...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|6|True|KIWEN !
jan Sisimi|7|True|KALAMA !
jan Sisimi|8|False|KIWEN !
jan Pepa|9|True|KIWEN !
jan Pepa|10|True|KALAMA !
jan Pepa|11|False|ale li ike, jan Pepa o.
jan Sisimi|12|True|jan Wasapi li wile lawa a e kulupu ante ale pi wawa nasa ...
jan Sisimi|13|True|sin pi tenpo suno la, ona en kulupu utala ona li tawa ma Tomopona...
jan Sisimi|14|False|ike a!
jan Sisimi|15|False|jan Kowijana a!
kalama|16|False|ma ona a!
jan Pepa|17|True|nasin wawa Molitawaa kin a!
jan Pepa|18|False|mi o toki a e ike ni tawa ona!
Rat|19|False|akesi sewi en jan lawa ona li awen tawa mi, li ken tawa e mi.
soweli Kawa|20|False|a, ilo lupa li kama pini. mi ken open!
Rat|21|False|Pini !
kalama|1|True|pona a!
kalama|2|True|mi kama e soweli Kawa, e len lawa mi.
kalama|3|True|muuuu!
kalama|4|True|AL As SA !
kalama|5|False|muuuuu!
jan Pepa|22|False|! ! !
jan Sisimi|23|False|! ! !
Guard|24|True|JAN AWEN O!
Guard|25|True|O KAMA!
Guard|26|False|JAN LI LON INSA LI WILE WEKA E JAN TAN POKI!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|False|a ni li suli: weka poki o kepeken tenpo lili taso...
jan Pepa|2|False|lon. tenpo lili taso.
jan Pepa|3|False|a, mi wile sona e ijo: sona sina la, jan Wasapi li pilin ike tawa mi tan seme?
jan Sisimi|4|True|ona li pilin ike tan jan pi wawa nasa lon nasin Pakalaa, jan Pepa o...
jan Sisimi|5|True|sina ken kama e mute suli la ni li ike tawa ona.
jan Sisimi|6|False|pilin ona la, pali ni li ken pakala a e pali ona pi tenpo kama.
jan Pepa|7|True|a. ni. ala a...
jan Pepa|8|False|ona li pilin ike tan ala; tenpo ala la mi kama pali e ni.
jan Sisimi|9|False|a ni li lon anu seme?
jan Pepa|10|False|ni a li lon a a a!
jan Sisimi|11|True|aa-aa!
jan Sisimi|12|False|pilin ona li tan ala...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|True|jan pali o!
jan lawa|2|False|sina sona la tomo ni li tomo pi jan nasa ike anu seme?
jan pali pi jan lawa|3|True|sona li wawa, jan lawa mi o!
jan pali pi jan lawa|4|False|jan lukin mi mute li lukin e ona lon ni.
jan lawa|5|True|GRrrr...
jan lawa|6|False|nasin tenpo mi la, musi utala mi la, ike li kama tan tomo ni a!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|False|mi tu li kulupu wan. ni li pona wawa la mi weka e tomo ni tan ma Elewa.
kalama|2|False|Luka
jan ike|3|False|p ona toki!
kulupu utala|4|True|pona!
kulupu utala|5|True|pona!
kulupu utala|6|True|pona!
kulupu utala|7|True|aaa!
kulupu utala|8|True|wawa a!
kulupu utala|9|True|pona!
kulupu utala|10|True|wawa !
kulupu utala|11|True|suli a!
kulupu utala|12|True|aaaa!
kulupu utala|13|False|pona a!
jan lawa|14|True|ILO PANA UTALA!
jan lawa|15|False|O PANA!!!
kalama|16|False|MuUUUUUUUUUUUUU !
kalama|17|True|Tawwwa!
kalama|18|False|Tawwaaa!
jan lawa|19|False|UTALA AAAAAA!!!
jan Pepa|20|True|seme li kama?
jan Pepa|21|False|utala anu seme?!
kalama|22|True|K ALA MA!
kalama|23|False|K A LA MA!
jan Sisimi|24|False|seme?!
kalama|25|True|PA A ~ K A L A !
kalama|26|False|P A K ALA !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|Jaki
jan Pepa|2|False|Jaki!!
jan Pepa|3|False|jan Sisimi o! sina pona ala pona?
jan Sisimi|4|True|mi pona!
jan Sisimi|5|True|sina sama ala sama?
jan Sisimi|6|False|soweli Kawa kin?
jan Pepa|7|False|mi pona.
jan Pepa|8|True|seme pakala a...
jan Pepa|9|False|lawa mi... li... ken ala...
jan Sisimi|10|True|jan utala ni li kama tan seme?!
jan Sisimi|11|True|li kepeken ilo pana utala a ?!
jan Sisimi|12|False|ona li wile e seme?
jan Pepa|13|True|mi sona ala...
jan Pepa|14|False|taso mi sona e ni tu .
jan Sisimi|15|False|?
jan Sisimi|16|False|! ! !
jan Sisimi|17|False|jan Toleja, mi lon ni!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Toleja|1|True|jan Sisimi o!
jan Toleja|2|False|sina pona awen la pona o tawa kon sewi a.
kalama|3|False|Jo
jan Toleja|4|True|sina lon poki awen la, mi pilin ike lon awen sina!
jan Toleja|5|True|utala ni kin!
jan Toleja|6|False|pakala mute!
jan Sisimi|7|False|jan Toleja o, mi pilin pona tan sina.
jan Pepa|8|True|namako a...
jan Pepa|9|True|jan ni pi lawa akesi li jan olin pi jan Sisimi...
jan Pepa|10|True|mi kama pakala e ona lon tenpo pini la ike taso li ken kama.
jan Pepa|11|True|kin la, kulupu utala li kama tawa mi.
jan Pepa|12|True|ante la mi kama ala weka tan poki awen.
jan Pepa|13|False|ale li sama nasin wan tawa pilin...
jan Pepa|14|False|...A!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Toleja|1|False|ona la seme li lon?
jan Sisimi|2|False|jan Pepa o? sina pilin ala pilin pona?
jan Pepa|3|True|pilin. mi pona.
jan Pepa|4|False|pini la, mi kama sona e ijo.
jan Pepa|5|True|kama ale ni li kama tan ijo mi tan kama pi pali mi tan wile mi... tan sona mi anu sona ala mi...
jan Pepa|6|False|...a lukin la, mi kama e mute suli!
jan Sisimi|7|True|a?
jan Sisimi|8|False|sina o toki e sona tawa mi.
jan Toleja|9|True|toki o pini. mi lon ma utala.
jan Toleja|10|True|mi tawa lon kon la, mi ken toki.
jan Toleja|11|False|o kama, jan Pepa o!
jan Sisimi|12|True|jan Toleja li sona wawa.
jan Sisimi|13|False|mi o tawa ma Tomopona. ni taso li nasin.
jan Pepa|14|False|o awen lili.
jan Pepa|15|True|kulupu utala pi jan Wasapi li kama utala kin.
jan Pepa|16|True|ona ale li moli la, ni li ike taso, li nasin mi ala.
jan Pepa|17|False|pilin mi la, pini utala o tan mi tan nasin mi.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Toleja|1|False|taso, kepeken nasin seme?
akesi Awa|2|True|lon a. sina wile pali ni kepeken nasin seme, jan pi wawa nasa o?
akesi Awa|3|False|wawa Lonaa sina li kama ala suli lon tenpo ni. mi ken pilin e ni.
jan Pepa|4|True|sona sina li lon. taso pali wawa wan mi li ken pona e ale.
jan Pepa|5|False|mi kama jo e wawa Lonaa sina la, mi ken.
akesi Awa|6|True|mi o pana e wawa tawa jan pi wawa nasa anu seme?
akesi Awa|7|True|ken ni li lawa ala!
akesi Awa|8|False|mi pali ala a!
jan Pepa|9|False|moli utala li pona tawa sina anu seme?
jan Toleja|10|True|akesi Awa o, ante e ken. ma utala ni la, jan utala ni en akesi utala ni li kulupu mi.
jan Toleja|11|False|li kulupu sina kin.
jan Sisimi|12|False|pona sina la o ni, akesi Awa o.
akesi Awa|13|True|AAA! ken!
akesi Awa|14|False|taso kama ale li tan ona taso!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|ko o o o o! !
jan Pepa|2|False|NAMAKO A!
jan Pepa|3|False|ni la mi sona e wawa Lonaa akesi!
jan Sisimi|4|False|jan Pepa o pali a! utala li lon!
jan Pepa|5|True|Sinus... !
jan Pepa|6|True|Wilus... !
jan Pepa|7|True|E... !
jan Pepa|8|True|Olinus...
jan Pepa|9|False|...TASOM !
kalama|10|False|Wwawaa!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|Iiinssa!
kalama|2|True|Jjjeloo!
kalama|3|True|Iinsaa!
kalama|4|True|Ssuno!
kalama|5|True|Iiinnsa!
kalama|6|True|Suuuno!
kalama|7|True|Innnsa!
kalama|8|True|Inssa!
kalama|9|True|Ssunoo!
kalama|10|False|Jjeelo!
jan Pepa|11|True|mi open e pali tawa pini pi nasin utala lon tenpo pini la, pali wawa ni li open.
jan Pepa|12|True|ona li ante e jan e pilin. jan ike sina li kama jan pona sina...
jan Pepa|13|False|…wile utala li kama wile olin li kama wile pona.
jan Sisimi|14|True|suli a!
jan Sisimi|15|True|a... ni li pona mute, jan Pepa o!
jan Sisimi|16|False|ona li pini e utala!
jan Toleja|17|False|ona li kama e pona!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|True|taso, seme li kama?
jan Sisimi|2|False|jan li uta e jan ante...?!
jan Toleja|3|True|a... ni li jan olin sin mute!
jan Toleja|4|False|ni li wile ala wile sina, jan Pepa o?
jan Pepa|5|False|a, ala! pilin mi la, wawa Lonaa akesi li wawa e olin pi wawa mi.
jan Toleja|6|False|a a, utala ni li kama awen lon lipu sona.
jan Sisimi|7|True|aaa-aa,
jan Sisimi|8|False|lon!
jan Pepa|9|False|aaaa, tan seme!
sitelen|10|False|- PINI -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo December 15, 2021 musi sitelen & toki: jan David Revoy. jan ni li pona e toki suli: jan Arlo James Barnes en jan Bhoren en jan Bobby Hiltz en jan Craig Maloney en jan Estefania de Vasconcellos Guimaraes en jan GunChleoc en jan Karl Ove Hufthammer en jan Nicolas Artance en jan Pierre-Antoine Angelini en jan Valvin. toki pona: jan Ke Tami li ante e toki. jan Juli en jan Tepo li lukin li pona e ona. jan ni li pona e toki nimi: jan Craig Maloney. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nicolas Artance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson ilo: ilo Krita 5.0β en ilo Inkscape 1.1 li kepeken ilo Kubuntu Linux 20.04. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
jan Pepa|2|False|sina sona ala sona?
jan Pepa|3|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free (libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1036 li pana e mani la, lipu ni li lon!
jan Pepa|5|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|6|False|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|7|False|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|8|False|sina pona!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
