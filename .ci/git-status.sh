#!/bin/bash
#
#  git-status.sh
#
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2019-2020 GunChleoc <fios@foramnagaidhlig.net>
#
#  Check that git history is clean
#

if [[ -z $(git status -s) ]]; then
    echo "###########################################"
    echo "  All files are up tp date :)"
    echo "###########################################"
else
    git status -s
    echo "###########################################"
    echo "Some files need updating!"
    echo "You can update all transcripts by running"
    echo "  0_transcripts/validate-transcripts.sh"
    echo "###########################################"
    exit 1
fi
