# Transcript of Pepper&Carrot Episode 06 [ru]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Эпизод 6: Конкурс зельеварения

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|О-о-ох, я снова уснула с открытым окном...
Перчинка|2|True|... как дует-то ...
Перчинка|3|False|... и почему мне видно из окна Комону?
Перчинка|4|False|КОМОНУ!
Перчинка|5|False|Конкурс зельеваров!
Перчинка|6|False|Должно быть, я... должно быть, случайно уснула*!
Перчинка|9|True|... но?
Перчинка|10|False|Где я ?!?
Bird|12|False|кря?|nowhitespace
Note|7|False|* см. эпизод 4 : Приступ гениальности|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|!!!
Перчинка|2|False|Морквик! Как мило, что ты позаботился о том, чтобы доставить меня на конкурс!
Перчинка|3|False|Фан-тас-ти-ка !
Перчинка|4|True|Ты даже додумался взять зелье, мою одежду и шляпу...
Перчинка|5|False|... ну-ка, глянем, какое зелье ты прихватил ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|ЧТО?!!
Mayor of Komona|3|False|На правах мэра Комоны, я объявляю соревнование зельеваров... Открытым!
Mayor of Komona|4|False|Наш город рад приветствовать на первом таком конкурсе аж четырёх ведьм!
Mayor of Komona|5|True|Давайте же им
Mayor of Komona|6|True|громко
Writing|2|False|Конкурс Зельеваров Комоны
Mayor of Komona|7|False|поаплодируем !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Хлоп
Mayor of Komona|1|True|Это честь видеть здесь восхитительную искусницу, прибывшую прямиком из Техносоюза -
Mayor of Komona|3|True|... не забывая и о нашей, собственной ведьме Комоны -
Mayor of Komona|5|True|... Наша третья участница - гостья из страны заходящих лун,
Mayor of Komona|7|True|... и наконец, наша последняя участница из леса Беличьего Уголка,
Mayor of Komona|2|False|Кориандру !
Mayor of Komona|4|False|Шафран !
Mayor of Komona|6|False|Ситими !
Mayor of Komona|8|False|Перчинка !
Mayor of Komona|9|True|И да начнутся игры!
Mayor of Komona|10|False|Голосовать будем аплодисментами
Mayor of Komona|11|False|Первой будет демонстрация Кориандры!
Кориандр|13|False|... Смерть больше не страшна благодаря моему ...
Кориандр|14|True|... Зелью
Кориандр|15|False|ЗОМБИФИКАЦИИ !
Audience|16|True|Хлоп
Audience|17|True|Хлоп
Audience|18|True|Хлоп
Audience|19|True|Хлоп
Audience|20|True|Хлоп
Audience|21|True|Хлоп
Audience|22|True|Хлоп
Audience|23|True|Хлоп
Audience|24|True|Хлоп
Audience|25|True|Хлоп
Audience|26|True|Хлоп
Audience|27|True|Хлоп
Audience|28|True|Хлоп
Кориандр|12|False|Дамы и господа...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|ВЕЛИКОЛЕПНО !
Audience|3|True|Хлоп
Audience|4|True|Хлоп
Audience|5|True|Хлоп
Audience|6|True|Хлоп
Audience|7|True|Хлоп
Audience|8|True|Хлоп
Audience|9|True|Хлоп
Audience|10|True|Хлоп
Audience|11|True|Хлоп
Audience|12|True|Хлоп
Audience|13|True|Хлоп
Audience|14|True|Хлоп
Audience|15|True|Хлоп
Audience|16|False|Хлоп
Шафран|18|True|Пора оценить
Шафран|17|True|... но пожалуйста, жители Комоны, приберегите свои аплодисменты !
Шафран|22|False|... и заставит их завидовать!
Шафран|19|True|МОЁ
Шафран|25|False|РОСКОШИ !
Шафран|24|True|... Зелья
Шафран|23|False|... и всё это возможно с помощью лишь одной капли моего ...
Audience|26|True|Хлоп
Audience|27|True|Хлоп
Audience|28|True|Хлоп
Audience|29|True|Хлоп
Audience|30|True|Хлоп
Audience|31|True|Хлоп
Audience|32|True|Хлоп
Audience|33|True|Хлоп
Audience|34|True|Хлоп
Audience|35|True|Хлоп
Audience|36|True|Хлоп
Audience|37|True|Хлоп
Audience|38|True|Хлоп
Audience|39|True|Хлоп
Audience|40|False|Хлоп
Mayor of Komona|42|False|Это зелье всю Комону может сделать богатой!
Mayor of Komona|41|True|Фантастически! Невероятно !
Audience|44|True|Хлоп
Audience|45|True|Хлоп
Audience|46|True|Хлоп
Audience|47|True|Хлоп
Audience|48|True|Хлоп
Audience|49|True|Хлоп
Audience|50|True|Хлоп
Audience|51|True|Хлоп
Audience|52|True|Хлоп
Audience|53|True|Хлоп
Audience|54|True|Хлоп
Audience|55|True|Хлоп
Audience|56|True|Хлоп
Audience|57|True|Хлоп
Audience|58|True|Хлоп
Audience|59|True|Хлоп
Audience|60|False|Хлоп
Mayor of Komona|2|False|Этим чу-до-дейст-вен-ным зельем Кориандра бросает вызов самой смерти!
Шафран|21|True|Зелье, которого вы все по-настоящему ждали: зелье, которое поразит ваших соседей ...
Mayor of Komona|43|False|Ваши аплодисменты не могут ошибаться. Кориандра уже вычеркнута из списка!
Шафран|20|False|зелье!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Ситими будет нелегко превзойти последнюю демонстрацию !
Ситими|4|True|НЕТ!
Ситими|5|True|Нельзя, это слишком опасно!
Ситими|6|False|Простите !
Mayor of Komona|3|False|... давай, Ситими, тебя все ждут!
Mayor of Komona|7|False|Похоже, дамы и господа, что Ситими лишается...
Шафран|8|False|Дай сюда !
Шафран|9|False|... и хватит притворяться стесняшкой, ты портишь всё шоу
Шафран|10|False|Какая разница, что там твоё зелье делает, всем уже и так понятно, что я выиграла конкурс ...
Ситими|11|False|!!!
Sound|12|False|вжжжЖЖУХ!|nowhitespace
Ситими|15|False|ГИГАНТСКИХ ЧУДОВИЩ !
Ситими|2|False|Я... Я не знала, что придется еще и демонстрировать...
Ситими|13|True|ОСТОРОЖНО!!!
Ситими|14|True|Это зелье

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|КО-КО-Коооооо|nowhitespace
Sound|2|False|БАМ!
Перчинка|3|True|... ха, класс !..
Перчинка|5|False|... во всяком случае, моё зелье удостоится пары смешков, потому что ....
Перчинка|4|False|теперь что, моя очередь ?
Mayor of Komona|6|True|Беги, дура!
Mayor of Komona|7|False|Конкурс окончен ! ...спасайся!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|2|False|... как обычно, когда наша очередь - все уходят ...
Перчинка|1|True|ну вот ...
Перчинка|4|True|Зато у меня хотя бы есть идея, как приспособить твое "зелье", Морквик
Перчинка|5|False|...давай тут всё уладим, и двинемся домой!
Перчинка|7|True|Ты,
Перчинка|8|False|модница-зомбоканарейка-переросток!
Перчинка|10|False|Хочешь попробовать последнее зелье? ...
Перчинка|11|False|... не очень, да ?
Перчинка|6|False|ЭЙ !
Sound|9|False|Хрясь !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|Ага, читай бумажку, внимательно читай ...
Перчинка|2|False|... Я не буду долго думать, прежде чем вылить его на тебя, если ты не уберёшься из Комоны сейчас же!
Mayor of Komona|3|True|За то, что спасла наш город, когда он был в опасности,
Mayor of Komona|4|False|мы награждаем первым местом Перчинку за ее Зелье... Зелье чего... ??!!
Перчинка|7|False|... ха... вообще-то, это вовсе не зелье; это баночка с анализами моего кота с последнего посещения ветеринара!
Перчинка|6|True|... Ха-ха! ну да ...
Перчинка|8|False|... так что, демонстрации не будет ?...
Narrator|9|False|Эпизод 6 : Конкурс зельеварения
Narrator|10|False|КОНЕЦ
Writing|5|False|50,000 Ko
Credits|11|False|Март 2015 - Сюжет и графика - David Revoy. Перевод - uncle Night

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot - полностью свободный и открытый комикс, спонсируемый благодаря добровольным взносам читателей. За этот эпизод спасибо 245 спонсорам:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Вы тоже можете стать спонсором Pepper&Carrot на следующий выпуск:
Credits|7|False|Инструменты : этот эпизод на 100% был нарисован в открытом программном обеспечении Krita на Linux Mint
Credits|6|False|Открытые исходные материалы: все исходные файлы доступны вместе с шрифтами на официальном сайте
Credits|5|False|Лицензия : Creative Commons Attribution Можно модифицировать, продавать, распространять, и т.п.. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
