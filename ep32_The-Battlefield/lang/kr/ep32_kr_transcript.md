# Transcript of Pepper&Carrot Episode 32 [kr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
제목|1|False|제32 화: 출정식

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
왕|1|True|반가워요!
왕|2|False|?!!
장군|3|True|난 제대로 된 전투용 마녀가 필요하다고!
장군|4|False|장군!
왕|5|False|내가 요구한 대로 마녀를 모집하였는가?
후추|6|True|그렇습니다, 폐하!
후추|7|False|폐하 옆에 대기하고 있사옵니다.
왕|8|False|...?
왕|9|True|제 이름은 후추...
왕|10|True|예끼 이놈!!!
왕|11|False|왜 이런 꼬맹이를 데리고 온 거야?!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|True|저기요!
후추|2|True|저 진짜 카오사 마녀 맞거든요.
후추|3|False|여기 보면 학위증도 있고...
글자|4|True|카오사
글자|5|False|학위증
글자|7|True|카옌
글자|8|False|커민
글자|6|True|백리향
글자|9|False|~ 후추에게 수여함 ~
왕|10|False|조용!
왕|11|True|우리 군대에 애들은 필요 없다.
왕|12|False|집에 가서 소꿉놀이나 해라.
소리|13|False|탁!
병사|14|True|하하하!
병사|15|True|하하하!
병사|16|True|하하하!
병사|17|False|아하하하!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|True|정말 믿기지가 않네!
후추|2|False|내 일생을 바쳐서 공부했는데, 사람들은 날 무시하고...
후추|3|False|...고작 내가 어려보인다는 거 때문에!
소리|4|False|푹! !|nowhitespace
후추|5|False|당근아 !|nowhitespace
소리|6|False|퍽 ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|True|그렇게 급했어, 당근?
후추|2|True|쩌적!
후추|3|False|그거라도 맛있게 먹어.
후추|4|False|꼴이 이게 뭐야...
후추|5|True|... 꼴이 ...
후추|6|False|인상착의!
소리|7|False|그거지!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|1|True|여기 누군가가 진짜 마녀 를 찾고 있다고 들었는데?!?
후추|2|False|어이!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
왕|1|True|다시 생각해 봤는데, 그 아이 다시 불러와라.
왕|2|False|이 자는 값이 너무 비쌀 것 같다.
글자|3|False|다음 화에 계속…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
후추|5|True|이번 화에 도움을 주신 1121명의 후원자분께 감사드립니다!
후추|3|True|후원은 Patreon, Tipeee, PayPal, Liberapay ...등으로 할 수 있어요!
후추|4|False|2020년 3월 31일 그림 및 이야기: David Revoy. 베타 독자: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. 한글판 번역: Jihoon Kim . 헤레바 세계관을 기반으로 함 원작자: David Revoy. 수석 감독: Craig Maloney. 작가: Craig Maloney, Nartance, Scribblemaniac, Valvin. 수정: Willem Sonke, Moini, Hali, CGand, Alex Gryson . 소프트웨어: 쿠분투 19.10에서 크리타 4.2.9-beta, 잉크스케이프 0.92.3 사용. 라이선스: 크리에이티브 커먼즈 저작자표시 4.0. www.peppercarrot.com
후추|7|True|여러분도 후추와 당근의 후원자가 되어서 여기에 이름을 올려보세요!
후추|6|True|후추와 당근은 완전히 자유 오픈소스이고 독자들의 후원을 받아 운영하고 있습니다.
후추|8|False|자세한 내용은 www.peppercarrot.com을 참고하세요!
후추|2|True|감사합니다!
크레딧|1|False|그거 아세요?
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
